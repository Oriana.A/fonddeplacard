
export class Ingredient {


    constructor(private name:string,
                private id?:number){}

    getName(){
        return this.name;
    }

    getId(){
        return this.id;
    }

    setName(name:string){
        this.name = name;
    }
    
    setId(id:number){
        this.id = id;
    }
}