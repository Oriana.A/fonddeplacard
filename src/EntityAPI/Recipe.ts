
export class Recipe {


    constructor(private name:string,
                private category:string,
                private picture:string,
                private score:number,
                private id?:number){}
 
    getName(){
        return this.name;
    }

    getCategory(){
        return this.category;
    }

    getPicture(){
        return this.picture;
    }

    getScore(){
        return this.score;
    }

    getId(){
        return this.id;
    }

    setName(name:string){
        this.name = name;
    }
    
    setCategory(category:string){
        this.category = category;
    }

    setPicture(picture:string){
        this.picture = picture;
    }

    setScore(score:number){
        this.score = score;
    }

    setId(id:number){
        this.id = id;
    }
}