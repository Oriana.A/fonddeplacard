import {createConnection} from "mysql";
import { promisify } from "util";

export class DatabaseConnexion {
    private query;
    private bdd = "apiFondDePlacard";
    private user = "simplon";
    private password = "1234";
    private host = "localhost";

    constructor() {        
        const databaseConnexion = createConnection({
            host:this.host,
            database:this.bdd,
            user: this.user,
            password: this.password
        });
        this.query = promisify(databaseConnexion.query).bind(databaseConnexion);
    }

    getQuery() {
        return this.query;
    }
}

