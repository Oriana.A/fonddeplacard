import { DatabaseConnexion } from "../Database/DatabaseConnexion"
import { Recipe } from "../EntityAPI/Recipe";
import { IRepository } from "./IRepository"


export class RecipeRepository implements IRepository {
    private query;
    //#region Ctor
    constructor() {
        //recuperation de la connection externalisée
        let databaseConnexion = new DatabaseConnexion();
        this.query = databaseConnexion.getQuery();
    }
    //#endregion

    //#region Method
    async getAll(): Promise<Recipe[]> {
        let result = await this.query("SELECT * FROM Recipe");
        let recipes = [];
        for (const row of result) {
            recipes.push(new Recipe(row['name'], row['category'], row['picture'], row['score'], row['id']));
        }
        return recipes;     
    }

    //Ne fonctionne pas 
    async create(recipe: Recipe): Promise<number> {
        let result = await this.query('INSERT INTO Recipe (name,category,picture,score) VALUES (?,?,?,?)', [
            recipe.getName(),
            recipe.getCategory(),
            recipe.getPicture(),
            recipe.getScore()
        ]);
        let id = result.insertId;
        recipe.setId(id);
        return id;
    }

    async getByName(name:string):Promise<Recipe>{
        let recipe = await this.query("")
        return recipe;
    }

    //A tester
    async delete(id: number) {
        await this.query("DELETE FROM Recipe WHERE id = ?",[id]);
    }

    //A tester
    async getById(id: number) {
        let recipe = await this.query("SELECT * FROM Recipe WHERE id = ?", [id]);
        return recipe;
    }

    /**
     * A tester - Revoir la requête et le passage des paramètres
     * Modify -> passer uniquement l'id ou l'objet ?
     * @param id 
     */
    async modify(id: number) {
        await this.query("UPDATE Recipe SET name = ?, category = ?, picture = ?, score = ? WHERE id = ?", [id]);
    }

    async getByIngredient(ingredient:string){
        throw new Error("Method not implemented.");
    }
    //#endregion
}
