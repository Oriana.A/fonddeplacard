
export interface IRepository{
    getAll();
    create(object:Object);
    delete(id:number);
    getById(id:number);
    getByName(name:string);
    getByIngredient(ingredient:string);
    modify(id:number);
}