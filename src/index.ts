import { RecipeController } from "./Controller/RecipeController";
import express from "express";


//#region Const
const app = express();
const port = 8080 || process.env.PORT;
const dao = new RecipeController();

//#endregion

//#region Method Expres
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
  });


app.get("/", async (req,res) => {
        let recipes = await dao.getAllRecipes()
        return res.json(recipes);
});

// Ne fonctionne pas : à retravailler
app.get("/recipe/create", async (req,res) => {
    dao.createRecipe(req.body).then(data => res.status(201).json(data));
});

//#endregion

//#region Mehtod Command Line
async function showAllRecipes() {
    let recipes = await dao.getAllRecipes();
    console.log(recipes);
}

showAllRecipes();
//#endregion


//#region Route 

/**
 * Comprendre comment les routes sont utilisées avec Express
 * La route "/" semble fonctionner
 */

// import { Recipe } from "./EntityAPI/Recipe";
// const cors = require('cors');
// const bodyParser = require('body-parser');
// const recipeRouter = require('./Controller/RecipeController');
// const bodyParser = require('body-parser');

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: false}));
// app.use(cors());

// app.use('/', recipeRouter);
// app.use('/recipe/create', recipeRouter)
//#endregion