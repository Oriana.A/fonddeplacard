import { Recipe } from '../EntityAPI/Recipe';
import { RecipeRepository} from '../Repository/RecipeRepository'

// const {Router} = require('express');
// const recipeRouter = Router();
// const recipeRepository = new RecipeRepository();

// recipeRouter.post('/recipe/create', async (req,res) => {
//     recipeRepository.create(req.body).then(data => res.status(201).json(data));
// });

// recipeRouter.get('/', async (req,res) => {
//     const recipes = await recipeRepository.getAll();
//     res.json(recipes);
// });

// module.exports = recipeRouter;

export class RecipeController{;
    private dao = new RecipeRepository();
    constructor(){}

    async getAllRecipes() { 
        let recipes = await this.dao.getAll();
        return recipes;
    }

    //Ne semble pas fonctionner
    async createRecipe(recipe:Recipe){
        let createdRecipe = await this.dao.create(recipe);
        return createdRecipe;
    } 
}