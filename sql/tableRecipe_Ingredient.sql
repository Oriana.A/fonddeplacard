USE apiFondDePlacard;
CREATE TABLE Recipe_Ingredient
(
	Id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	RecipeID INT NOT NULL,
    IngredientID INT NOT NULL,
    CONSTRAINT `fk_RecipeId_Recipe_Id`
    FOREIGN KEY (RecipeId) REFERENCES Recipe (Id)
    ON DELETE CASCADE,
    CONSTRAINT `fk_IngredientId_Ingredient_Id`
    FOREIGN KEY (IngredientId) REFERENCES Ingredient (Id)
    ON DELETE CASCADE
)